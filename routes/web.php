<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::resource('/','ProjectController');

Route::get('/project/{id}','ProjectController@showProject');

Route::get('/architects','ArchitectController@index');

Route::get('/architects/{id}','ArchitectController@showOne');

Route::get('filter/{filter}','ProjectController@filterProject');

Route::get('/ajax-projects/{id}','AjaxController@moreProjects');
Route::get('ajax-studio/{studio}/{id}','AjaxController@moreProjectStudio');

//Middleware

Route::get('/admin', 'AdminController@index');

Route::get('/admin/project','AdminController@allProjects');
Route::get('/admin/project/edit','AdminController@addProject');
Route::post('/admin/project/store','AdminController@storeProject');
Route::get('/admin/project/{id}','AdminController@oneProject');
Route::post('/admin/project/update/{id}','AdminController@updateProject');
Route::delete('/admin/project/delete/{id}','AdminController@deleteProject');

Route::get('/admin/architect','AdminController@allArchitects');
Route::get('/admin/architect/edit','AdminController@addArchitect');
Route::post('/admin/architect/store','AdminController@storeArchitect');
Route::get('/admin/company/{id}','AdminController@oneArchitect');
Route::post('/admin/company/update/{id}','AdminController@updateArchitect');






Route::get('/lang/{type}','ProjectController@langChose');
Route::get('/gal','AdminController@changeImages');

