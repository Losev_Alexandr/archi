/*

 1, 2, 3, 5, 8, 13, 16, 21, 26, 34, 42, 55, 68, 89, 110, 144, 178, 233, 288, 377, 610, 987, 1597, 2584

 */


$(document).ready(function(){

    //----------------------------
    // Ajax "more projects"
    $('.add-more-projects a').click(function(){
        var last_project_id = $('.more-projects .post:nth-last-child(2)').attr('data-id');

        $.ajax({
            url: '/ajax-projects/'+last_project_id,
            type: "GET", // not POST, laravel won't allow it
            dataType:'json',
            success: function(data){

                var arch = data.architect;
                var proj =data.more_projects;
                var gallery = data.galleries;

                var countPojects = (data.more_projects.length);
                console.log(data.more_projects);
                if (countPojects != 6){
                    $('.add-more-projects').css({
                        display:'none'
                    })
                }

                $.each(proj,function(key,value){

                    var id_company = value.id_company;
                    var id_project = value.id;
                    var proj_name = value.name;
                    var proj_descriprion = value.description;
                    var proj_descriprion_ru = value.description_ru;
                    var main_img = value.main_img;
                    var archName = getArch(id_company);  
                    var oneGallery = projGallery(id_project);  
                    var printProject = addProjectInView (id_company,id_project,proj_name,proj_descriprion,proj_descriprion_ru,archName,oneGallery,main_img);

                    setTimeout(function(){
                        $('.more-projects > .clearfix').before(printProject);

                    },key*400);

                });

                function getArch(id){
                    var main_value='';
                    $.each(arch,function(key,value){
                        if(value.id ==id){
                            main_value=value.name;
                        }
                    });
                    return main_value;
                }

                function projGallery (id){
                    var arrayGal=[];
                    $.each(gallery,function(key,value){
                        if(value.project_id == id){
                            arrayGal[arrayGal.length]=value.img_url;
                        }
                    });
                    return arrayGal;
                }

                function addProjectInView (id_company,id_project,proj_name,proj_descriprion,descriprion_ru,archName,oneGallery,main_img){

                    var content='';

                    content+='                    <div class="post col-3" data-id="'+id_project+'">';
                    content+='                         <div class="post-img rsize-img">';
                    content+='                            <a href="/project/'+id_project+'"><img src="'+main_img+'" alt="'+proj_name+'"></a>';
                    content+='                    </div>';
                    content+='                    <div class="post-gallery rsize-img">';
                    for(var i =0;i<3;i++){
                        var galImg = oneGallery[i];
                        galImg = galImg.substr(9);
                        galImg = '/uploads/crop_190x120_'+galImg;
                        content+='<div class="col-3"><a href="/project/'+id_project+'"><img src="'+galImg+'" alt="'+proj_name+'"></a></div>';
                    }
                    content+='                        <div class="clearfix"></div>';
                    content+='                    </div>';
                    content+='                    <div class="post-description">';
                    content+='                        <a href="/project/'+id_project+'"><h5>'+proj_name+'</h5></a>';
                    content+='                        <a href="/architects/'+id_company+'"><h6>'+archName+'</h6></a>';
                    content+='                        <p>'+descriprion_ru+'</p>';
                    content+='                    </div>';
                    content+='                </div>';

                    return content;
                }
            }
        });
    });

    $('.add-more-projects-studio a').click(function(){
        var last_project_id = $('.more-projects .post:nth-last-child(2)').attr('data-id');
        var company_id = $(this).attr('data-studio-id');


        $.ajax({
            url: '/ajax-studio/'+company_id+'/'+last_project_id,
            type: "GET", // not POST, laravel won't allow it
            dataType:'json',
            success: function(data){

                var arch = data.architect;
                var proj =data.more_projects;
                var gallery = data.galleries;

                var countPojects = (data.more_projects.length); // ???????? ??? ?????? (???? ?? 6 ????????? , ?? ?????? ??????????)
                console.log(data.more_projects);
                if (countPojects != 6){
                    $('.add-more-projects-studio').css({
                        display:'none'
                    })
                }

                $.each(proj,function(key,value){

                    var id_company = value.id_company;
                    var id_project = value.id;
                    var proj_name = value.name;
                    var proj_descriprion = value.description;
                    var proj_descriprion_ru = value.description_ru;
                    var main_img = value.main_img;
                    var archName = getArch(id_company);  // ??????? ???????? ???????????/??????
                    var oneGallery = projGallery(id_project);  // ??????? ???????? ?????? ??????? ? ????????? ? ??????
                    var printProject = addProjectInView (id_company,id_project,proj_name,proj_descriprion,proj_descriprion_ru,archName,oneGallery,main_img);

                    setTimeout(function(){
                        $('.more-projects > .clearfix').before(printProject);

                    },key*400);

                });

                function getArch(id){
                    var main_value='';
                    $.each(arch,function(key,value){
                        if(value.id ==id){
                            main_value=value.name;
                        }
                    });
                    return main_value;
                }

                function projGallery (id){
                    var arrayGal=[];
                    $.each(gallery,function(key,value){
                        if(value.project_id == id){
                            arrayGal[arrayGal.length]=value.img_url;
                        }
                    });
                    return arrayGal;
                }

                function addProjectInView (id_company,id_project,proj_name,proj_descriprion,descriprion_ru,archName,oneGallery,main_img){

                    var content='';

                    content+='                    <div class="post col-3" data-id="'+id_project+'">';
                    content+='                         <div class="post-img rsize-img">';
                    content+='                            <a href="/project/'+id_project+'"><img src="'+main_img+'" alt="'+proj_name+'"></a>';
                    content+='                    </div>';
                    content+='                    <div class="post-gallery rsize-img">';
                    for(var i =0;i<3;i++){
                        var galImg = oneGallery[i];
                        galImg = galImg.substr(9);
                        galImg = '/uploads/crop_190x120_'+galImg;
                        content+='<div class="col-3"><a href="/project/'+id_project+'"><img src="'+galImg+'" alt="'+proj_name+'"></a></div>';
                    }
                    content+='                        <div class="clearfix"></div>';
                    content+='                    </div>';
                    content+='                    <div class="post-description">';
                    content+='                        <a href="/project/'+id_project+'"><h5>'+proj_name+'</h5></a>';
                    content+='                        <a href="/architects/'+id_company+'"><h6>'+archName+'</h6></a>';
                    content+='                        <p>'+descriprion_ru+'</p>';
                    content+='                    </div>';
                    content+='                </div>';

                    return content;
                }
            }
        });
    });
    // End Ajax "more projects"
    //----------------------------


    //--------------
    // PRELOADER

    var img_count = $('img').length;
    var one_img_percent = 100 / img_count;
    var load_progress = 0;
    //var images_load = 0;
    if(img_count>0){
        $('.preloader div.line').css({
            'height':'5px',
            'background-color':'#1BCA8D',
            'width':'0%',
            'transition': '.8s'
        });
    }
    $('img').load(function(){
        load_progress += one_img_percent;
        if(load_progress >= 100){
            $('.preloader').css('opacity',0).css('display','none');
        }
        $('.preloader div.line').css('width', load_progress + '%');
    });
    $(window).load(function(){
        $('.preloader div.line').css('width','100%');
        setTimeout(function(){
            $('.preloader').css('opacity',0).css('display','none');
        },750);
    });

    //  END PRELOADER
    //-----------------

    resizeImg('.post-img img',0.62);
    resizeImg('.post-gallery img',0.62);
    resizeImg('header.company',0.42);
    resizeSlider();
    borderHeight();

        //Slider
    
        $(window).bind('load', function () {
            var sliderWrp = $('.loop');
            if(sliderWrp.length>0) {
            sliderWrp.owlCarousel({
                center: true,
                items: 1,
                loop: true,
                margin: 3,
                autoWidth: true,
                responsive: {
                    600: {
                        items: 3
                    }
                },
                navigation: true
            });
            }
        });
    


    // Project Page

    $arch_element=$('.resize-text p');
    $arch_content = $arch_element.html();
    $arch_charCount = 390;
    
    if($arch_content){
    if($arch_content.length>$arch_charCount){
        contentCharResize($arch_element,$arch_content,$arch_charCount);
        borderHeight();
    }}

    //-------------------
    // Gallery

    var arrayGallery = [];
    var count=1;
    $('#owl-example .item img').each(function(){

        var heightOrWidth='';
        //if($(this).height() > $(this).width()){
        //    //heightOrWidth = 'height:100%';
        //}else{
        //    //heightOrWidth = 'width:100%';
        //}


        var newImg = '<img class="gall-item" src="'+$(this).attr('src')+'" alt="gallery image" style="'+heightOrWidth+';" data-img-number="'+count+'">';
        $('.gallery .gallery-img').append(newImg);





        // ?????????? ? ?????????? ??????

        //var imgHeight = $(this).height();
        //var imgWidth = $(this).width();
        //
        //arrayGallery[count]=[];
        //arrayGallery[count][0]=$(this).attr('src');
        //if(imgHeight > imgWidth){
        //    arrayGallery[count][1]=true;
        //}else{
        //    arrayGallery[count][1]=false;
        //}
        count++;

    });

    var clickMouse, imgAtr;

    $('#owl-example .item img').mousedown(function(e){
        return clickMouse=e.pageX;
    });
    $('#owl-example .item img').mouseup(function(e){
        var upMouse=e.pageX;
        //console.log(clickMouse-upMouse);
        if(Math.abs(clickMouse-upMouse)<15){
            $('header , main').css('filter','blur(5px)');
            $('.gallery').fadeIn(250);
            var openImgSrc = $(this).attr('src');
            $('.gallery-img img[src="'+openImgSrc+'"]').fadeIn(300);
            return imgAtr=$('.gallery-img img[src="'+openImgSrc+'"]').attr('data-img-number');
        }

    });

    // Close gallery

    $('.gallery-img').click(function(){
        $('.gallery').fadeOut(250,function(){
            $('.gallery-img img').css('display','none');
        });
        $('header , main').css('filter','');
    });

    $('.gal-ar').click(function(e){
        var arrow =$(this).attr('data-arrow');
        if(arrow == 'next'){
            $('.gallery-img img[data-img-number="'+imgAtr+'"]').fadeOut(300);
            ++imgAtr;
            if(imgAtr>count-1){
                imgAtr=1;
            }
            $('.gallery-img img[data-img-number="'+imgAtr+'"]').delay(300).fadeIn(300);
        }else if(arrow == 'prev'){
            $('.gallery-img img[data-img-number="'+imgAtr+'"]').fadeOut(300);
            --imgAtr;
            if(imgAtr<1){
                imgAtr=count-1;
            }
            $('.gallery-img img[data-img-number="'+imgAtr+'"]').delay(300).fadeIn(300);
        }
    });
    $('.gallery-img img').click(function(event){
        event.preventDefault();
        event.stopPropagation();
        $('.gallery-img img[data-img-number="'+imgAtr+'"]').fadeOut(300);
        ++imgAtr;
        if(imgAtr>count-1){
            imgAtr=1;
        }
        $('.gallery-img img[data-img-number="'+imgAtr+'"]').delay(300).fadeIn(300);

    });

    //var newImg = '<img src="'+arrayGallery[1]+'" alt="gallery image 1">';
    //$('.gallery .gallery-img').html(newImg);

    // End Gallery
    //-------------------

});

$(".next").click(function(){
    owl.trigger('#owl-example.next');
});
$(".prev").click(function(){
    owl.trigger('#owl-example.prev');
});
$(window).resize(function(){
    resizeImg('.post-img img',0.62);
    resizeImg('.post-gallery img',0.62);
    resizeImg('header.company',0.42);
    resizeSlider();
    borderHeight();
});
function resizeImg (element, step){
    var width = $(element).width();
    $(element).css('height', (width*step));
}
function resizeSlider (){
    var window_width= $(window).width();
    //var slide_width= $('.project-carusel .item img').width();
    //var slide_kof = window_width/slide_width;
    $('.project-carusel .item img').css({
        height:window_width/2.4
    });
    $('#owl-example').css('height',window_width/2.4);
}
function borderHeight(){
    $('.about-project .project-company').css('min-height',$('.about-project .project-decription').height());
}

function contentCharResize ($element,$content,$countChar){
    $new_content = $content.substring(0,$countChar);
    var lang = $('.lang').text();
    var lang_trim = $.trim(lang);
    if(lang_trim=='en'){
        $new_content =$new_content+'<span class="more_text"> ... more </span>';
    }else{
        $new_content =$new_content+'<span class="more_text"> ... полностью </span>';
    }

    $element.html($new_content);
}
function contentCharAll($element,$content){
    var lang = $('.lang').text();
    var lang_trim = $.trim(lang);
    if(lang_trim=='en'){
        $new_content= $content+'<span class="hide_text"> #hide </span>';
    }else{
        $new_content= $content+'<span class="hide_text"> #скрыть</span>';
    }

    $element.html($new_content);
}

$('.menu-trigger').on('click', function(){
    $('.slide-burger-menu').toggleClass('active');
    $('.burger-menu').toggleClass('active');
    $(this).toggleClass('active');
});

$('.lang').click(function(){
    $('.chose-lang').toggleClass('active');
});


// Click

$('.resize-text').on('click', '.more_text',function(){
    contentCharAll($arch_element,$arch_content);
});

$('.resize-text').on('click', '.hide_text',function(){
    contentCharResize($arch_element,$arch_content,$arch_charCount);
});



