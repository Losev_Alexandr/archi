<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRuFieldArchitectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('architects', function (Blueprint $table) {
            $table->text('name_ru');
            $table->text('description_ru');
            $table->text('country_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('architects', function (Blueprint $table) {
            $table->dropColumn('name_ru');
            $table->dropColumn('description_ru');
            $table->dropColumn('country_ru');
        });
    }
}
