<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('id_company');
            $table->string('name', 100);
            $table->text('architects');
            $table->text('project_team');
            $table->float('area');
            $table->smallInteger('year');
            $table->text('photographer');
            $table->text('description');
            $table->text('country');
            $table->text('main_img');
            $table->smallInteger('id_gallery');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
