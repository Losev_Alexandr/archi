<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRuFielProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('name_ru', 100);
            $table->text('architects_ru');
            $table->text('project_team_ru');
            $table->text('photographer_ru');
            $table->text('description_ru');
            $table->text('country_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('name_ru');
            $table->dropColumn('architects_ru');
            $table->dropColumn('project_team_ru');
            $table->dropColumn('photographer_ru');
            $table->dropColumn('description_ru');
            $table->dropColumn('country_ru');
        });
    }
}
