<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required_with:architects,project_team,photographer,description|required_without:name_ru',
            'description' => 'required_with:name',
            'name_ru' => 'required_with:architects_ru,project_team_ru,photographer_ru,description_ru|required_without:name',
            'description_ru' => 'required_with:name_ru',
            'country' => 'required',
            'year' => 'required|numeric|max:2030|min:1800',
            'area' => 'required',
        ];
    }
}
