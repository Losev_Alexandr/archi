<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArchitectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_url'=>'required|url',
            'phone_number'=>'required',
            'name'=>'required',
            'country'=>'required',
            'description'=>'required',
            'description_ru'=>'required'
        ];
    }
}
