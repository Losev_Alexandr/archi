<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArhitectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected $rules=[
        'site_url'=>'required|url',
        'phone_number'=>'required',
        'name'=>'required',
        'country'=>'required',
        'description'=>'required',
        'backgroundImage'=>'required',
        'logoImage'=>'required',
    ];

    public function rules()
    {
        $rules = $this->rules;
        return $rules;
    }
}
