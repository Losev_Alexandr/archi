<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use App\Architect;
use App\Gallery;
use App\ProjectFiltr;
use Session;

class AjaxController extends Controller
{
    function lang()
    {
        $lang=Session::get('lang');
        switch ($lang){
            case 'en':
                $lang='description';
                break;
            case 'ru':
                $lang='description_ru';
                break;
        }
        return $lang;
    }

    public function moreProjects($id)
    {
        $lang = $this->lang();

        $more_projects = Project::where($lang, '!=', '')->where('id', '<', $id)->orderBy('id', 'desc')->limit(6)->get();

        $array_company = array_pluck($more_projects,'id_company'); //������ � ������������� ������������ ��������

        $array_gallery = array_pluck($more_projects,'id'); //������ id ��������

        $architect = Architect::select('id', 'name')->wherein('id', $array_company)->orderBy('id', 'desc')->get(); // ������ id -> �������� ��������

        $gallery = Gallery::wherein('project_id', $array_gallery)->orderBy('id', 'desc')->get(); // ������ ���� �������� �� ����������� ��������

        return response()->json([
            'galleries' => $gallery,
            'architect' => $architect,
            'more_projects' => $more_projects
        ]);

    }

    public function moreProjectStudio($studio,$id)
    {
        $lang = $this->lang();

        $more_projects = Project::where($lang, '!=', '')->where('id_company','=',$studio)->where('id', '<', $id)->orderBy('id','desc')->limit(6)->get();

        $array_gallery = array_pluck($more_projects,'id'); //������ id ��������

        $architect = Architect::select('id', 'name')->where('id', $studio)->get(); // ������ id -> �������� ��������

        $gallery = Gallery::wherein('project_id', $array_gallery)->orderBy('id', 'desc')->get(); // ������ ���� �������� �� ����������� ��������

        return response()->json([
            'galleries' => $gallery,
            'architect' => $architect,
            'more_projects' => $more_projects
        ]);

    }

    public function moreProjectFiltr($filtr,$id)
    {
        $lang = $this->lang();

        $array_filters = ProjectFiltr::select('project_id')->where('filter','=',$filtr)->get();

        dd($array_filters);

        $more_projects = Project::where($lang, '!=', '')->where('id_company','=',$studio)->where('id', '<', $id)->orderBy('id','desc')->limit(6)->get();

        $array_gallery = array_pluck($more_projects,'id'); //������ id ��������

        $architect = Architect::select('id', 'name')->where('id', $studio)->get(); // ������ id -> �������� ��������

        $gallery = Gallery::wherein('project_id', $array_gallery)->orderBy('id', 'desc')->get(); // ������ ���� �������� �� ����������� ��������

        return response()->json([
            'galleries' => $gallery,
            'architect' => $architect,
            'more_projects' => $more_projects
        ]);

    }
}
