<?php

namespace App\Http\Controllers;

use App\Architect;
use App\Gallery;
use App\Http\Requests;
use App\Project;
use App\ProjectFiltr;
use Image;
use Redirect;
use Session;
use Validator;

//use App\Http\Requests;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $architects = Architect::orderBy('id','desc')->get();

        return view('admin.index',compact('projects','architects'));
    }

    public function allProjects(){
        return 'ok';
    }

    public function addProject(){
        return view('admin.addProject');
    }

    public function storeProject(Requests\ProjectRequest $request){
        // dd($request);
        $data = $request->all();
        $file = $request->file('image');
        $oldaname = $file->getClientOriginalName();
        $ex = $file->getClientOriginalExtension();
        $filename = md5(time() . $oldaname) . '.' . $ex;
        $file->move(public_path().'/uploads', $filename);
        $data['main_img']='/uploads/'.$filename;

        // Resize, crop main image and  save
        $dst_name_main=md5(time().$oldaname);
        $this->resizeImages($data['main_img'], $dst_name_main, 585, 365);
        $this->resizeImages($data['main_img'], $dst_name_main, 190, 120);

        $data['country_ru']='none';

        //          Add to Gallery
        $prj = Project::create($data);
        if($prj){
            $last_id = $prj->id;
            $images=$request->file('images');
            foreach ($images as $img){
                $old_name_img = $img->getClientOriginalName();
                $img_ex= $img->getClientOriginalExtension();
                $new_name_img= '/uploads/'.md5(time().$old_name_img).'.'.$img_ex;
                $new = md5(time().$old_name_img).'.'.$img_ex;
                $img->move(public_path().'/uploads', $new_name_img);
                Gallery::insertGetId(array('user_id'=>'1','img_url'=>$new_name_img,'project_id'=>$last_id));

//                print_r($new_name_img);
//                print_r($new);
//                dd($img);

//                 Resize, crop images and  save
                $dst_name=md5(time().$old_name_img);
                $this->resizeImages($new_name_img, $dst_name, 585, 365);
                $this->resizeImages($new_name_img, $dst_name, 190, 120);

                Image::make(public_path().'/uploads/'.$new)->heighten(800, function ($constraint) {
                    $constraint->upsize();
                })->save(public_path().'/uploads'.'/crop_h800_'.$new);
            }

            //      Add to Filters
            $filters= $request->filter;
            foreach($filters as $filter){
                ProjectFiltr::insertGetId(array('filter'=>$filter,'project_id'=>$last_id));
            }
        }

        return redirect('/admin/company/'.$prj->id_company)->with('message','The project has been successfully added');
    }

    public function resizeImages($img_src, $dst, $width, $height)
    {
        //Get image size
//        $url_img=$_SERVER['SERVER_NAME'].$img_src;
//        $url_img=$_SERVER['DOCUMENT_ROOT'].'uploads'.$img_src;
//        dd($img_src);
        $url_img = public_path().'/'.$img_src;
        list($width_orig, $height_orig) = getimagesize($url_img);
        // Aspect ratio
        $ratio = min($width_orig / $width, $height_orig / $height);

        $src_w = $width * $ratio;
        $src_h = $height * $ratio;

        $src_x = 0;
        $src_y = 0;

        if ($ratio == $width_orig / $width) {
            $src_y = ($height_orig / $ratio - $height) / 2;
        } else {
            $src_x = ($width_orig / $ratio - $width) / 2;
        }
        $new = imagecreatetruecolor($width, $height);

//        dd($url_img);
//                       dd($src_y);
        $new_image = imagecreatefromjpeg($url_img);

        imagecopyresampled($new, $new_image, 0, 0, $src_x, $src_y, $width, $height, $src_w, $src_h);
        imagejpeg($new, "./uploads/crop_".$width."x".$height."_".$dst.".jpg");
    }

    public function oneProject($id){
        $project = Project::where('id',$id)->first();

        return view('admin.oneProject',compact('project'));
    }

    public function updateProject(Requests\ProjectUpdateRequest $request , $id){

        $data = $request->all();
        $file = $request->file('image');
        $images=$request->file('images');

        if($file){
            $oldaname = $file->getClientOriginalName();
            $ex = $file->getClientOriginalExtension();
            $filename = md5(time() . $oldaname) . '.' . $ex;
            $file->move(public_path().'/uploads', $filename);
            $data['main_img']='/uploads/'.$filename;

            // Resize, crop main image and  save
            $dst_name_main=md5(time().$oldaname);
            $this->resizeImages($data['main_img'], $dst_name_main, 585, 365);
            $this->resizeImages($data['main_img'], $dst_name_main, 190, 120);
        }

        //        Add to Gallery
        if($images){
            $last_id = $id;
            Gallery::where('project_id',$id)->delete();
            foreach ($images as $img){
                $old_name_img = $img->getClientOriginalName();
                $img_ex= $img->getClientOriginalExtension();
                $new_name_img= '/uploads/'.md5(time().$old_name_img).'.'.$img_ex;
                $new = md5(time().$old_name_img).'.'.$img_ex;
                $img->move(public_path().'/uploads', $new_name_img);
                Gallery::insertGetId(array('user_id'=>'1','img_url'=>$new_name_img,'project_id'=>$last_id));

                // Resize, crop images and  save
                $dst_name=md5(time().$old_name_img);

                $this->resizeImages($new_name_img, $dst_name, 585, 365);
                $this->resizeImages($new_name_img, $dst_name, 190, 120);

                Image::make(public_path().'/uploads/'.$new)->heighten(800, function ($constraint) {
                    $constraint->upsize();
                })->save(public_path().'/uploads'.'/crop_h800_'.$new);
            }
        }

        //      Add to Filters
        if($request->filter){
            $filters= $request->filter;
            foreach($filters as $filter){
                ProjectFiltr::where('project_id',$id)->delete();
                ProjectFiltr::insertGetId(array('filter'=>$filter,'project_id'=>$id));
            }
        }

        //-------------------

        Project::find($id)->update($data);
        $id_company=Project::where('id',$id)->select('id_company')->first();

        return redirect(url('/admin/company/'.$id_company['id_company']))->with('message','Project "'.$request->name.'" has been updated successfully');
    }

    public function deleteProject($id){
        $project_name=Project::select('id','name','id_company')->where('id',$id)->first();
        Project::find($id)->delete();
        ProjectFiltr::where('project_id',$id)->delete();

        return redirect(url('/admin/company/'.$project_name->id_company))->with('message','Project "'.$project_name->name.'" has been deleted successfully');
    }

    public function oneArchitect($id){
        $projects = Project::orderBy('id','desc')->where('id_company',$id)->get();
        $arch = Architect::where('id',$id)->first();

        return view('admin.oneArchitect',compact('arch','projects'));
    }

    public function addArchitect(){
        return view('admin.addArchitect');
    }

    public function storeArchitect(Requests\ArhitectRequest $request){

        $data = $request->all();

        //save crop backgrond
        $img = $data['backgroundImage'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $decodeImg= base64_decode($img);
        $jpeg_bg_name = "bg-".time().".jpeg";
        $path = public_path() . "/uploads/" . $jpeg_bg_name;
        file_put_contents($path, $decodeImg);

        //save crop logo
        $img_l = $data['logoImage'];
        $img_l = str_replace('data:image/png;base64,', '', $img_l);
        $img_l = str_replace(' ', '+', $img_l);
        $decodeImg_l= base64_decode($img_l);
        $jpeg_l_name = "logo-".time().".jpeg";
        $path_l = public_path() . "/uploads/" . $jpeg_l_name;
        file_put_contents($path_l, $decodeImg_l);

        $data['logo_url']='/uploads/'.$jpeg_l_name;
        $data['main_img_url']='/uploads/'.$jpeg_bg_name;
//        $data['email']='none';
//        $data['name_ru']='none';
//        $data['country_ru']='none';

        unset($data['bgImage'],$data['logoImage'],$data['backgroundImage'],$data['logoImg'],$data['glocality'],$data['gcountry']);

        //dd($data);
        Architect::create($data);

        return redirect('/admin');
    }

    public function updateArchitect(Requests\ArchitectUpdateRequest $request ,$id){

        $data = $request->all();

        //----------------- Start Crop ----------------
        if($data['backgroundImage']){
            //save crop backgrond
            $img = $data['backgroundImage'];
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $decodeImg= base64_decode($img);
            $jpeg_bg_name = "bg-".time().".jpeg";
            $path = public_path() . "/uploads/" . $jpeg_bg_name;
            file_put_contents($path, $decodeImg);

            $data['main_img_url']='/uploads/'.$jpeg_bg_name;
        }
        if($data['logoImage']){
            //save crop logo
            $img_l = $data['logoImage'];
            $img_l = str_replace('data:image/png;base64,', '', $img_l);
            $img_l = str_replace(' ', '+', $img_l);
            $decodeImg_l= base64_decode($img_l);
            $jpeg_l_name = "logo-".time().".jpeg";
            $path_l = public_path() . "/uploads/" . $jpeg_l_name;
            file_put_contents($path_l, $decodeImg_l);

            $data['logo_url']='/uploads/'.$jpeg_l_name;
        }
        //----------------- End Crop -------------------

        Architect::find($id)->update($data);

        return redirect(url('/admin/company/'.$id))->with('message','Company has been updated successfully');
    }

    public function changeImages(){
        $allImages = Gallery::select('img_url','id')->get();
        $counter=0;
        foreach ($allImages as $urlimage){
            $fileURL = $_SERVER['DOCUMENT_ROOT'].$urlimage->img_url;
            if(file_exists($fileURL)){
                if(!strstr($fileURL,'.jpg',true)){
//                    dd($urlimage->img_url);
                    $new_url=$urlimage->img_url.'.jpg';
                    //resize and crop
                    $dst_name= substr(strstr($urlimage->img_url,'ds/'),3);
                    $this->resizeImages($urlimage->img_url, $dst_name, 585, 365);
                    $this->resizeImages($urlimage->img_url, $dst_name, 190, 120);
                    Gallery::where('id',$urlimage->id)->update(['img_url'=>$new_url]);

                    ++$counter;
                }
            }
        }
//        dd($counter);
    }

    //Change image size for main image in gallery
    public function changeImages2(){
        $allImages = Project::select('main_img','id')->get();
//        dd($allImages);
        $counter=0;
        foreach ($allImages as $urlimage){
            $fileURL = $_SERVER['DOCUMENT_ROOT'].$urlimage->main_img;
            if(file_exists($fileURL)){
                if ($urlimage->main_img == '/uploads/c2fc5789d35cecfecff0b57459de2bc2.jpg') {

                    $inmg = substr($urlimage->main_img, 9);

                    //$new_url=$urlimage->img_url;
                    //resize and crop
                    $this->resizeImages2($urlimage->main_img, $inmg, 585, 365);
                    $this->resizeImages2($urlimage->main_img, $inmg, 190, 120);
                    //Project::where('id',$urlimage->id)->update(['main_img'=>$new_url]);
                    ++$counter;
                }
            }
        }
        dd($counter);
    }

    public function resizeImages2($img_src , $dst , $width , $height){
        //Get image size
        $url_img=$_SERVER['SERVER_NAME'].$img_src;
//        dd($url_img);
        list($width_orig, $height_orig) = getimagesize('http://'.$url_img);
        // Aspect ratio
        $ratio = min($width_orig/$width,$height_orig/$height);

        $src_w=$width*$ratio;
        $src_h=$height*$ratio;

        $src_x=0;
        $src_y=0;
        if($ratio==$width_orig/$width){
            $src_y= ($height_orig/$ratio - $height)/2;
        }else{
            $src_x= ($width_orig/$ratio - $width)/2;
        }

        $new = imagecreatetruecolor($width,$height);
        //dd('http://'.$url_img);
        $new_image = imagecreatefromjpeg('http://'.$url_img);
        //dd($url_img);
        imagecopyresampled($new, $new_image, 0, 0, $src_x,500, $width, $height, $src_w, $src_h);
        //dd($dst);
        imagejpeg( $new,"./uploads/crop_".$width."x".$height."_".$dst );
    }
}
