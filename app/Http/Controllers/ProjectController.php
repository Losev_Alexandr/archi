<?php

namespace App\Http\Controllers;

use App\Project;
use App\ProjectFiltr;
use Illuminate\Http\Request;
use Image;
use Redirect;
use Session;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $country_ru = ['ru','os','ua','by','md','kg','kz','lv','ro','bg'];


        if(!Session::has('lang')){
            foreach ($country_ru as $country){
                if($country==$lang){
                    Session::put('lang', $lang);
                    break;
                }else{
                    Session::put('lang', 'en');
                }
            }
        }
}

    public function index()
    {
        $lang = $this->lang();
        $projects = Project::where($lang, '!=', '')->orderBy('id', 'desc')->paginate(12);

        /* foreach($projects as $p){
             dd($p->gallery);
         }*/

        return view('projects.index', compact('projects'));
    }

    function lang()
    {
        $lang=Session::get('lang');
        switch ($lang){
            case 'en':
                $lang='name';
                break;
            case 'ru':
                $lang='name_ru';
                break;
        }
        return $lang;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function showProject($id){
        $lang=$this->lang();
        $projects = Project::where('id',$id)->first();
        $projects_one_page = Project::where($lang,'!=','')->where('id','<',$id)->orderBy('id','desc')->limit(7)->get();
        $imgResize = [];
//        dd($projects->allImgGallery);
        foreach ($projects->allImgGallery as $img) {
            $image = Image::make(public_path().$img->img_url);
            $imgResize[] = $image->heighten(595, function ($constraint) {
                $constraint->upsize();
            });
        }

//        dd($imgResize);

        return view('projects.one_project', compact('projects', 'projects_one_page', 'imgResize'));
    }

    public function updateImages()
    {
        $files = scandir(public_path().'/uploads/t/2');
        $directory = public_path().'/uploads/t/2';
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        foreach ($files as $img) {

//            print_r($img.'<br>');
            if ($img == '.' || $img == '..') {
            } else {

                $res = Image::make($directory.'/'.$img)->heighten(800, function ($constraint) {
                    $constraint->upsize();
                })->save($directory.'/crop_h800_'.$img);
//                print_r($res);
                if ($res) {
                }
            }
            // Function to rename images to .jpg
//            $firstname = explode('.', $img);
//            if(count($firstname)==1){
//                echo '<br>1<br>';
//                echo $img;
//                rename(public_path().'/uploads/'.$img, public_path().'/uploads/'.$img.'.jpg');
//            }
//            else{
//                echo '<br>2<br><br>';
//                echo $img;
//            }
        }
    }

    public function filterProject($filter){
        $filter_num ='';
        switch ($filter){
            case 'house':
                $filter_num =1; break;
            case 'apartments':
                $filter_num=2; break;
            case 'restaurants':
                $filter_num=3; break;
            case 'studio':
                $filter_num=4; break;
            case 'hotels':
                $filter_num=5; break;
            case 'offices':
                $filter_num=6; break;
        }
        $filters = ProjectFiltr::where('filter',$filter_num)->orderBy('id','desc')->paginate(12);

        return view('projects.filter_index',compact('filters'));
    }

    public function langChose ($type){
        Session::put('lang',$type);
        return Redirect::back();

    }
}
