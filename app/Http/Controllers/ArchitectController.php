<?php

namespace App\Http\Controllers;

use App\Architect;
use Illuminate\Http\Request;
use App\Project;
use Session;

use App\Http\Requests;

class ArchitectController extends Controller
{
    public function __construct() {
        if(!Session::has('lang')){
            Session::put('lang', 'en');
        }
    }

    function lang()
    {
        $lang=Session::get('lang');
        switch ($lang){
            case 'en':
                $lang='name';
                break;
            case 'ru':
                $lang='name_ru';
                break;
        }
        return $lang;
    }

    public function index(){
        return View('architects.index');
    }

    public function showOne($id){
        $lang=$this->lang();
        $arch=Architect::where('id', $id)->first();
        $projects = Project::where($lang,'!=','')->where('id_company',$id)->orderBy('id','desc')->limit(6)->get();
        //dd($projects);
        //['arch'=>$arch,'projects'=>$projects]
        return view('architects.one_arch',compact('arch','projects'));
    }
}
