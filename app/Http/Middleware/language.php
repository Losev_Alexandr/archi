<?php

namespace App\Http\Middleware;
use Session;
use Closure;

class language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        app()->setLocale(Session::get('lang'));
        return $next($request);
    }
}
