<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Architect extends Model
{
    protected $fillable=['name','description','main_img_url','logo_url','country','phone_number','site_url','email','name_ru','description_ru','country_ru','facebook_link','instagram_link','vk_link'];

    protected $attributes = [
        'email' => 'info@archi.world',
        'name_ru' => 'info@archi.world',
        'country_ru' => 'info@archi.world',
    ];
    
    public function getProject(){
        return $this->hasMany('App\Project','id_company','id');
    }

    public function projects(){
        return $this->hasMany('App\Project','id_company','id');
    }
}
