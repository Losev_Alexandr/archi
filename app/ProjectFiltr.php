<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class ProjectFiltr extends Model
{
    function lang()
    {
        $lang=Session::get('lang');
        switch ($lang){
            case 'en':
                $lang='name';
                break;
            case 'ru':
                $lang='name_ru';
                break;
        }
        return $lang;
    }

    //filters:
    // 1- House
    // 2- Apartments
    // 3- Restaurants
    // 4- Studio
    protected $fillable=['filter','project_id'];

    public function project(){
        $lang = $this -> lang();
        return $this->hasOne('App\Project','id','project_id');
    }
}
