<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable=['id_company','name','architects','project_team','area','year','photographer','description','country','main_img','id_gallery','name_ru','architects_ru','project_team_ru','photographer_ru','description_ru','country_ru'];

    public function allImgGallery(){
        return $this->hasMany('App\Gallery','project_id','id');
    }

    public function gallery(){
        return $this->hasMany('App\Gallery','project_id','id')->limit(3);
    }

    public function architect(){
        return $this->hasOne('App\Architect','id','id_company')->select('name','id','logo_url','description','description_ru');
        //   return $this->belongsTo(Architect::class);
    }

    public function projectFilter(){
        return $this->hasMany('App\ProjectFitr','project_id','id');
    }
}
