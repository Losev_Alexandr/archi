<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('main.name', 'Architecture main') }}</title>
    <style type="text/css" rev="stylesheet">
        /*Preloader*/
        .preloader{
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: white;
            z-index: 9000;
            opacity: 1;
            width: 100%;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>

    {{--GoogleAnalytics--}}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-91158171-1', 'auto');
        ga('send', 'pageview');

    </script>
    {{-- End GoogleAnalytics--}}
</head>
<body class="padding-0-55">
<div class="preloader">
    <div class="line"></div>
</div>
{{app()->setLocale(Session::get('lang'))}}
<div class="slide-burger-menu">


    {{--SOCIAL BUTTONS--}}

    {{--<div class="social-menu">--}}
        {{--<a href="#"><div></div><img src="/svg/facebook-logo.svg"></a>--}}
        {{--<a href="#"><img src="/svg/google-plus.svg"></a>--}}
        {{--<a href="#"><div></div><img src="/svg/instagram-logo.svg"></a>--}}
        {{--<a href="#"><div></div><img src="/svg/vk-logo.svg"></a>--}}
    {{--</div>--}}


    <div class="menu">
        <ul>
            {{app()->setLocale(Session::get('lang'))}}
            <li><a href="{{url('/filter/house')}}">{{trans('language.House')}}</a></li>
            <li><a href="{{url('/filter/apartments')}}">{{trans('language.Apartments')}}</a></li>
            <li><a href="{{url('/filter/restaurants')}}">{{trans('language.Restaurants')}}</a></li>
            <li><a href="{{url('/filter/hotels')}}">{{trans('language.Hotels')}}</a></li>
            <li><a href="{{url('/filter/offices')}}">{{trans('language.Offices')}}</a></li>
            {{--<li><a href="{{url('/filter/studio')}}">{{trans('language.Studio')}}</a></li>--}}
            {{--<li><a href="#">{{trans('language.Contacts')}}</a></li>--}}
        </ul>
        {{--<div class="button-follow add-project">--}}
            {{--<a href="#">{{trans('language.Add Project')}}</a>--}}
        {{--</div>--}}
    </div>
</div>
<header>
    <section>
        <div class="header-logo">
            <a href="/">
                <img src="{{url('/svg/logo-A.svg')}}" alt="architecture">
            </a>
        </div>


        {{--SEARCH--}}

        {{--<div class="header-search">--}}
        {{--<form>--}}
        {{--<input type="text">--}}
        {{--</form>--}}
        {{--<img src="/svg/search-A.svg" alt="serach">--}}
        {{--</div>--}}


        <div class="burger-menu">
            <div class="menu-trigger second">
                <span class="line line-1"></span>
                <span class="line line-2"></span>
                <span class="line line-3"></span>
            </div>
        </div>
        <div class="language">
            <div class="lang">
                @if (Session::has('lang'))
                    {{Session::get('lang')}}
                @else
                    EN
                @endif
            </div>
            <div class="chose-lang">
                <a href="{{url('/lang/en')}}">EN</a>
                <a href="{{url('/lang/ru')}}">RU</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
</header>
<main>
    @yield('content')
</main>
<footer></footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/script.js"></script></body>
</html>
