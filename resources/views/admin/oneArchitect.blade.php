@extends('layouts.admin')

@section('content')
    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="padding: 25px">
                    @if(Session::has('message'))
                        <div class="alert alert-success"> {{Session::get('message')}}</div>
                    @endif
                    {{app()->setLocale(Session::get('lang'))}}
                    <div class="panel-heading" style="font-size: 21px">
                        {{$arch->name}}
                    </div>
                    <div><a href="{{url('/admin/project/edit?id='.$arch->id)}}"><button type="button" class="btn btn-success" style="margin-top: 20px;">{{trans('language.Add new project')}}</button></a></div>
                    <div class="panel-body">
                        {!! Form::model($arch,array('url' => ['/admin/company/update',$arch->id],'files' => true,'method'=>'POST')) !!}

                        {{--------------------------}}
                        {{--------------------------}}

                        <div class="col-md-6">
                            <h3>{{trans('language.English version')}}</h3>

                            <div class="form-group col-md-12">
                                {!! Form::label('name',trans('language.Name Studio')) !!}
                                @if($errors->has('name'))
                                    {!! Form::text('name',null,['class'=>"form-control error",'placeholder'=>'Введите название компании (это поле обязательное)']) !!}
                                @else
                                    {!! Form::text('name', null,['class'=>"form-control"]) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('country',trans('language.Country')) !!}
                                @if($errors->has('country'))
                                    {!! Form::text('country', null,['class'=>'form-control error','placeholder'=>'Введите страну (это поле обязательное)']) !!}
                                @else
                                    {!! Form::text('country', null,['class'=>'form-control']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {{ Form::label('description',trans('language.Description')) }}
                                @if($errors->has('description'))
                                    {!! Form::textarea('description', null,['class'=>'form-control error','rows'=>'16','placeholder'=>'Введите описание компании (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('description', null,['class'=>'form-control','rows'=>'16']) !!}
                                @endif
                            </div>

                            {{--Fields for filters--}}

                            {!! Form::hidden('glocality', null,['id'=>'glocality']) !!}
                            {!! Form::hidden('gcountry', null,['id'=>'gcountry']) !!}
                        </div>

                        <div class="col-md-6">
                            <h3>{{trans('language.Russian version')}}</h3>

                            <div class="form-group col-md-12">
                                {{ Form::label('description_ru',trans('language.Description')) }}
                                @if($errors->has('description_ru'))
                                    {!! Form::textarea('description_ru', null,['class'=>'form-control error','rows'=>'16','placeholder'=>'Введите описание компании (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('description_ru', null,['class'=>'form-control','rows'=>'16']) !!}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>

                            <div class="form-group col-md-6">
                                {!! Form::label('phone_number',trans('language.Contacts')) !!}
                                @if($errors->has('phone_number'))
                                    {!! Form::textarea('phone_number', null,['class'=>'form-control error','rows'=>'16','wrap'=>'soft','placeholder'=>'Введите данные о компании (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('phone_number', null,['class'=>'form-control']) !!}
                                @endif
                            </div>

                            {{-------------------------------}}
                            {{-----------START CROPP---------}}

                            <div class="col-md-12">
                                <div class="col-md-6 background-company-img image-crop">
                                    <div id="crop-background-company">

                                        <!-- Current avatar -->
                                        <div class="avatar-view background-company" title="Change main image company">

                                            <p>{{trans('language.Background image')}}</p>

                                            <img src="{{$arch->main_img_url}}" alt="Background company" class="uploadImg">   {{------}}
                                        </div>

                                        <!-- Cropping modal -->
                                        <div class="modal fade" id="background-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title" id="avatar-modal-label">Add main image</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="avatar-body">

                                                            <!-- Crop and preview -->
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="avatar-wrapper"></div>
                                                                </div>
                                                            </div>

                                                            <div class="row avatar-btns">
                                                                <div class="col-md-9">
                                                                    <input type="file" class="avatar-input" id="bgImage" name="bgImage">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    {{--<button type="submit" class="btn btn-primary btn-block avatar-save">Add</button>--}}
                                                                    <div class="crop">Crop</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="modal-footer">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div><!-- /.modal -->

                                        <!-- Loading state -->
                                        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                    </div>
                                </div>

                                <div class="col-md-6 logo-company-img image-crop">
                                    <div id="crop-logo-company">

                                        <!-- Current avatar -->
                                        <div class="avatar-view logo-company" title="Change logo company">

                                            <p>{{trans('language.Logo image')}}</p>

                                            <img src="{{$arch->logo_url}}" alt="Logo company" class="uploadImg">
                                        </div>

                                        <!-- Cropping modal -->
                                        <div class="modal fade" id="logo-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title" id="avatar-modal-label">Add logo image</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="avatar-body">

                                                            <!-- Crop and preview -->
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="avatar-wrapper"></div>
                                                                </div>
                                                            </div>

                                                            <div class="row avatar-btns">
                                                                <div class="col-md-9">
                                                                    <input type="file" class="avatar-input" id="logoImg" name="logoImg">
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <div class="crop">Crop</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div><!-- /.modal -->

                                        <!-- Loading state -->
                                        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                    </div>
                                </div>
                            </div>


                            {!! Form::hidden('backgroundImage', null,['class'=>'form-control','id'=>'backgroundImage']) !!}
                            {!! Form::hidden('logoImage', null,['class'=>'form-control','id'=>'logoImage']) !!}

                            {{------------END CROPP----------}}
                            {{-------------------------------}}

                            <div class="col-md-6">
                                <div class="form-group col-md-12 ">
                                    {!! Form::label('site_url',trans('language.Site url')) !!}
                                    @if($errors->has('site_url'))
                                        {!! Form::text('site_url', null,['class'=>'form-control error','placeholder'=>'Введите URL сайта (это поле обязательное)']) !!}
                                    @else
                                        {!! Form::text('site_url', null,['class'=>'form-control']) !!}
                                    @endif
                                </div>

                                <div class="form-group col-md-12">
                                    {!! Form::label('facebook_link',trans('language.Link on Facebook')) !!}
                                    {!! Form::text('facebook_link', null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group col-md-12">
                                    {!! Form::label('instagram_link',trans('language.Link on Instagram')) !!}
                                    {!! Form::text('instagram_link', null,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group col-md-12">
                                    {!! Form::label('vk_link',trans('language.Link on Vkontakte')) !!}
                                    {!! Form::text('vk_link', null,['class'=>'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group col-md-2">
                                {!! Form::label('', '') !!}
                                {!! Form::button(trans('language.Update'), ['type'=>'submit','class'=>'btn btn-primary btn-block']) !!}
                            </div>
                        </div>
                        {{--------------------------}}
                        {{--------------------------}}

                        {!! Form::close() !!}
                    </div>
                </div>
                {{--{{dd($projects)}}--}}
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table">
                            <thead class="thead-inverse">
                            <tr>
                                <th>#id</th>
                                <th></th>
                                <th>{{trans('language.Name Project')}}</th>
                                <th>{{trans('language.Project URL')}}</th>
                                <th>{{trans('language.Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($projects as $project)
                                    <tr>
                                        <th scope="row">{{$project->id}}</th>
                                        <td><img src="{{$project->main_img}}" style="height: 90px;width: 90px;object-fit: cover"></td>
                                        <td>{{$project->name}}</td>
                                        <td><a href="{{url('/')}}/project/{{$project->id}}">{{trans('language.Go to project')}}</a></td>
                                        <td>
                                            {!! Form::open(array('url' => ['/admin/project/delete',$project->id],'method'=>'DELETE')) !!}
                                                <a href="{{url('/admin/project/'.$project->id)}}"><button type="button" class="btn btn-info">{{trans('language.Change project')}}</button></a>
                                                {!! Form::button(trans('language.Delete'), ['type'=>'submit','class'=>'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Go to company--}}

    <a href="{{url('/architects/'.$arch->id)}}" class="change_content_button">
        <div>
            <img src="/svg/go-back.svg" alt="{{trans('language.Go to company')}}">
        </div>
    </a>
    {{--------------    -------------------}}
@endsection