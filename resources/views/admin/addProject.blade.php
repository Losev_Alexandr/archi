@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {{app()->setLocale(Session::get('lang'))}}
                <div class="panel panel-default">
                    <h2 class="panel-body">{{trans('language.Add new project')}}</h2>
                </div>
                {!! Form::open(array('url' => '/admin/project/store','files' => true)) !!}
                <div class="col-md-6" style="padding-top: 20px;">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-size: 21px">{{trans('language.English version')}}</div>
                        @if(Session::has('message'))
                            <div class="alert alert-success"> {{Session::get('message')}}</div>
                        @endif
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                {!! Form::label('name', trans('language.Name Project')) !!}
                                @if($errors->has('name'))
                                    {!! Form::text('name',null,['class'=>"form-control error",'placeholder'=>'Введите название компании (это поле обязательное)']) !!}
                                @else
                                    {!! Form::text('name', null,['class'=>'form-control']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12" style="display: none;">
                                {!! Form::text('id_company',$_GET["id"],['class'=>'form-control']) !!}
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('architects',  trans('language.Architects')) !!}
                                @if($errors->has('architects'))
                                    {!! Form::textarea('architects',null,['class'=>"form-control error",'rows'=>'3','placeholder'=>'Введите архитекторов (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('architects', null,['class'=>'form-control','rows'=>'3']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('project_team', trans('language.Project Team')) !!}
                                @if($errors->has('project_team'))
                                    {!! Form::textarea('project_team',null,['class'=>"form-control error",'rows'=>'3','placeholder'=>'Введите команду (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('project_team', null,['class'=>'form-control','rows'=>'3']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('photographer', trans('language.Photographs')) !!}
                                @if($errors->has('photographer'))
                                    {!! Form::textarea('photographer',null,['class'=>"form-control error",'rows'=>'3','placeholder'=>'Введите фотографов (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('photographer', null,['class'=>'form-control','rows'=>'3']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('description', trans('language.Description project')) !!}
                                @if($errors->has('description'))
                                    {!! Form::textarea('description',null,['class'=>"form-control error",'rows'=>'16','placeholder'=>'Введите описание (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('description', null,['class'=>'form-control','rows'=>'16']) !!}
                                @endif
                            </div>

                            {{--Fields for filters--}}

                            {!! Form::hidden('glocality', null,['id'=>'glocality']) !!}
                            {!! Form::hidden('gcountry', null,['id'=>'gcountry']) !!}
                        </div>
                    </div>
                </div>

                {{---------------}}

                <div class="col-md-6" style="padding-top: 20px;">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-size: 21px">{{trans('language.Russian version')}}</div>
                        <div class="panel-body">
                            <div class="form-group col-md-12">
                                {!! Form::label('name_ru', trans('language.Name Project')) !!}
                                @if($errors->has('name_ru'))
                                    {!! Form::text('name_ru',null,['class'=>"form-control error",'placeholder'=>'Введите название компании (это поле обязательное)']) !!}
                                @else
                                    {!! Form::text('name_ru', null,['class'=>'form-control']) !!}
                                @endif
                            </div>

                            {{--<div class="form-group col-md-12">--}}
                                {{--{!! Form::label('country_ru', trans('language.Country')) !!}--}}
                                {{--@if($errors->has('country_ru'))--}}
                                    {{--{!! Form::text('country_ru',null,['class'=>"form-control error",'placeholder'=>'Введите место расположения (это поле обязательное)','readonly' => 'true']) !!}--}}
                                {{--@else--}}
                                    {{--{!! Form::text('country_ru', null,['class'=>'form-control','readonly' => 'true']) !!}--}}
                                {{--@endif--}}
                            {{--</div>--}}

                            <div class="form-group col-md-12">
                                {!! Form::label('architects_ru', trans('language.Architects')) !!}
                                @if($errors->has('architects_ru'))
                                    {!! Form::textarea('architects_ru',null,['class'=>"form-control error",'rows'=>'3','placeholder'=>'Введите архитекторов (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('architects_ru', null,['class'=>'form-control','rows'=>'3']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('project_team_ru', trans('language.Project Team')) !!}
                                @if($errors->has('project_team_ru'))
                                    {!! Form::textarea('project_team_ru',null,['class'=>"form-control error",'rows'=>'3','placeholder'=>'Введите команду (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('project_team_ru', null,['class'=>'form-control','rows'=>'3']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('photographer_ru', trans('language.Photographs')) !!}
                                @if($errors->has('photographer_ru'))
                                    {!! Form::textarea('photographer_ru',null,['class'=>"form-control error",'rows'=>'3','placeholder'=>'Введите фотографов (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('photographer_ru', null,['class'=>'form-control','rows'=>'3']) !!}
                                @endif
                            </div>

                            <div class="form-group col-md-12">
                                {!! Form::label('description_ru', trans('language.Description project')) !!}
                                @if($errors->has('description_ru'))
                                    {!! Form::textarea('description_ru',null,['class'=>"form-control error",'rows'=>'16','placeholder'=>'Введите описание (это поле обязательное)']) !!}
                                @else
                                    {!! Form::textarea('description_ru', null,['class'=>'form-control','rows'=>'16']) !!}
                                @endif
                            </div>

                        </div>
                    </div>
                </div>

                {{-------------}}

                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="form-group">
                            {!!Form::checkbox('filter[]', '1') !!}
                            {!! Form::label('filter[]', trans('language.House')) !!}
                            {!!Form::checkbox('filter[]', '2') !!}
                            {!! Form::label('filter[]', trans('language.Apartments')) !!}
                            {!!Form::checkbox('filter[]', '3') !!}
                            {!! Form::label('filter[]', trans('language.Restaurants')) !!}
                            {!!Form::checkbox('filter[]', '5') !!}
                            {!! Form::label('filter[]', trans('language.Hotels')) !!}
                            {!!Form::checkbox('filter[]', '6') !!}
                            {!! Form::label('filter[]', trans('language.Offices')) !!}
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('country', trans('language.Country')) !!}
                            @if($errors->has('country'))
                                {!! Form::text('country',null,['class'=>"form-control error",'placeholder'=>'Введите место расположения (это поле обязательное)']) !!}
                            @else
                                {!! Form::text('country', null,['class'=>'form-control']) !!}
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('year', trans('language.Project Year')) !!}
                            @if($errors->has('year'))
                                {!! Form::text('year',null,['class'=>"form-control error",'placeholder'=>'Введите год проекта (это поле обязательное)']) !!}
                            @else
                                {!! Form::text('year', null,['class'=>'form-control']) !!}
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('area', trans('language.Area')) !!}
                            @if($errors->has('area'))
                                {!! Form::text('area',null,['class'=>"form-control error",'placeholder'=>'Введите площадь (это поле обязательное)']) !!}
                            @else
                                {!! Form::text('area', null,['class'=>'form-control']) !!}
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('main_img', trans('language.Main image')) !!}
                            {!! Form::file('image') !!}
                        </div>

                        <div class="form-group col-md-6">
                            {!! Form::label('id_gallery', trans('language.Gallery')) !!}
                            {!! Form::file('images[]', array('multiple'=>true)) !!}
                        </div>

                        <div class="form-group col-md-3">
                            {!! Form::label('', '') !!}
                            {!! Form::button(trans('language.Create'), ['type'=>'submit','class'=>'btn btn-primary btn-block']) !!}
                        </div>

                        <div>
                            @if($errors->has('image')||$errors->has('images'))
                                {{trans('language.Main image')}} and {{trans('language.Gallery')}} required
                            @endif
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
                {{---------------}}

            </div>
        </div>
        {{--@if (count($errors) > 0)--}}
            {{--<div class="alert alert-danger">--}}
                {{--<ul>--}}
                    {{--@foreach ($errors->all() as $error)--}}
                        {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--@endif--}}
    </div>
@endsection