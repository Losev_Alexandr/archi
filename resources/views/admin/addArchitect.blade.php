@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="padding: 25px">
                    {{app()->setLocale(Session::get('lang'))}}
                    <div class="panel-heading" style="font-size: 21px">
                        <div class="col-md-12">
                            {{trans('language.Add new Studio')}}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        {!! Form::open(array('url' => '/admin/architect/store','files' => true)) !!}


                            <div class="col-md-6">
                                <h3>{{trans('language.English version')}}</h3>

                                <div class="form-group col-md-12">
                                    {!! Form::label('name',trans('language.Name Studio')) !!}
                                    @if($errors->has('name'))
                                        {!! Form::text('name',null,['class'=>"form-control error",'placeholder'=>'Введите название компании (это поле обязательное)']) !!}
                                    @else
                                        {!! Form::text('name', null,['class'=>"form-control"]) !!}
                                    @endif
                                </div>

                                <div class="form-group col-md-12">
                                    {!! Form::label('country',trans('language.Country')) !!}
                                    @if($errors->has('country'))
                                        {!! Form::text('country', null,['class'=>'form-control error','placeholder'=>'Введите страну (это поле обязательное)']) !!}
                                    @else
                                        {!! Form::text('country', null,['class'=>'form-control']) !!}
                                    @endif
                                </div>

                                <div class="form-group col-md-12">
                                    {!! Form::label('description',trans('language.Description')) !!}
                                    @if($errors->has('description'))
                                        {!! Form::textarea('description', null,['class'=>'form-control error','rows'=>'16','placeholder'=>'Введите описание компании (это поле обязательное)']) !!}
                                    @else
                                        {!! Form::textarea('description', null,['class'=>'form-control','rows'=>'16']) !!}
                                    @endif
                                </div>

                                    {{--Fields for filters--}}

                                    {!! Form::hidden('glocality', null,['id'=>'glocality']) !!}
                                    {!! Form::hidden('gcountry', null,['id'=>'gcountry']) !!}
                            </div>

                            <div class="col-md-6">

                                <h3>{{trans('language.Russian version')}}</h3>

                                <div class="form-group col-md-12">
                                    {!! Form::label('description_ru',trans('language.Description')) !!}
                                    @if($errors->has('description_ru'))
                                        {!! Form::textarea('description_ru', null,['class'=>'form-control error','rows'=>'16','placeholder'=>'Введите описание компании (это поле обязательное)']) !!}
                                    @else
                                        {!! Form::textarea('description_ru', null,['class'=>'form-control','rows'=>'16']) !!}
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>

                                <div class="form-group col-md-6">
                                    {!! Form::label('phone_number',trans('language.Contacts')) !!}
                                    @if($errors->has('phone_number'))
                                        {!! Form::textarea('phone_number', null,['class'=>'form-control error','rows'=>'16','wrap'=>'soft','placeholder'=>'Введите данные о компании (это поле обязательное)']) !!}
                                    @else
                                        {!! Form::textarea('phone_number', null,['class'=>'form-control']) !!}
                                    @endif
                                </div>

                                {{--<div class="col-md-6">--}}
                                    {{--<div class="form-group col-md-12">--}}
                                        {{--{!! Form::label('main_img_url',trans('language.Background image')) !!}--}}
                                        {{--@if($errors->has('bg_image'))--}}
                                            {{--{!! Form::file('bg_image') !!}--}}
                                        {{--@else--}}
                                            {{--{!! Form::file('bg_image') !!}--}}
                                        {{--@endif--}}
                                    {{--</div>--}}

                                    {{--<div class="form-group col-md-12">--}}
                                        {{--{!! Form::label('logo_url',trans('language.Logo image')) !!}--}}
                                        {{--@if($errors->has('logo_image'))--}}
                                            {{--{!! Form::file('logo_image') !!}--}}
                                        {{--@else--}}
                                            {{--{!! Form::file('logo_image') !!}--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                    {{--<div style="color: #bc2417">{{trans('language.Background image')}} and {{trans('language.Logo image')}} required</div>--}}
                                {{--</div>--}}

                                {{-------------------------------}}
                                {{-----------START CROPP---------}}


                                <div class="col-md-12">
                                    <div class="col-md-6 background-company-img image-crop">
                                        <div id="crop-background-company">

                                            <!-- Current avatar -->
                                            <div class="avatar-view background-company" title="Change main image company">

                                                <p>{{trans('language.Background image')}}</p>

                                                <img src="/svg/upload.svg" alt="Avatar">
                                            </div>

                                            <!-- Cropping modal -->
                                            <div class="modal fade" id="background-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title" id="avatar-modal-label">Add main image</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="avatar-body">

                                                                <!-- Crop and preview -->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="avatar-wrapper"></div>
                                                                    </div>
                                                                </div>

                                                                <div class="row avatar-btns">
                                                                    <div class="col-md-9">
                                                                        <input type="file" class="avatar-input" id="bgImage" name="bgImage">
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        {{--<button type="submit" class="btn btn-primary btn-block avatar-save">Add</button>--}}
                                                                        <div class="crop">Crop</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div><!-- /.modal -->

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 logo-company-img image-crop">
                                        <div id="crop-logo-company">

                                            <!-- Current avatar -->
                                            <div class="avatar-view logo-company" title="Change logo company">

                                                <p>{{trans('language.Logo image')}}</p>

                                                <img src="/svg/upload.svg" alt="Logo company">
                                            </div>

                                            <!-- Cropping modal -->
                                            <div class="modal fade" id="logo-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title" id="avatar-modal-label">Add logo image</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="avatar-body">

                                                                <!-- Crop and preview -->
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="avatar-wrapper"></div>
                                                                    </div>
                                                                </div>

                                                                <div class="row avatar-btns">
                                                                    <div class="col-md-9">
                                                                        <input type="file" class="avatar-input" id="logoImg" name="logoImg">
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        {{--<button type="submit" class="btn btn-primary btn-block avatar-save">Add</button>--}}
                                                                        <div class="crop">Crop</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div><!-- /.modal -->

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                    </div>
                                </div>


                                {!! Form::hidden('backgroundImage', null,['class'=>'form-control','id'=>'backgroundImage']) !!}
                                {!! Form::hidden('logoImage', null,['class'=>'form-control','id'=>'logoImage']) !!}


                                {{------------END CROPP----------}}
                                {{-------------------------------}}


                                <div class="col-md-6">
                                    <div class="form-group col-md-12 ">
                                        {!! Form::label('site_url',trans('language.Site url')) !!}
                                        @if($errors->has('site_url'))
                                            {!! Form::text('site_url', null,['class'=>'form-control error','placeholder'=>'Введите URL сайта (это поле обязательное)']) !!}
                                        @else
                                            {!! Form::text('site_url', null,['class'=>'form-control']) !!}
                                        @endif
                                    </div>

                                    <div class="form-group col-md-12">
                                        {!! Form::label('facebook_link',trans('language.Link on Facebook')) !!}
                                        {!! Form::text('facebook_link', null,['class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group col-md-12">
                                        {!! Form::label('instagram_link',trans('language.Link on Instagram')) !!}
                                        {!! Form::text('instagram_link', null,['class'=>'form-control']) !!}
                                    </div>
                                    <div class="form-group col-md-12">
                                        {!! Form::label('vk_link',trans('language.Link on Vkontakte')) !!}
                                        {!! Form::text('vk_link', null,['class'=>'form-control']) !!}
                                    </div>

                                </div>


                                <div class="form-group col-md-2">
                                    {!! Form::label('', '') !!}
                                    {!! Form::button(trans('language.Create'), ['type'=>'submit','class'=>'btn btn-primary btn-block']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>

                {{--@if (count($errors) > 0)--}}
                    {{--<div class="alert alert-danger">--}}
                        {{--<ul>--}}
                            {{--@foreach ($errors->all() as $error)--}}
                                {{--<li>{{ $error }}</li>--}}
                            {{--@endforeach--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--@endif--}}
            </div>
        </div>
    </div>
@endsection