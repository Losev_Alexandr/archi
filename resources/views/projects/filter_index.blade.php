@extends('layouts.main')

@section('content')
    <section class="main-wrapper">

        @foreach($filters as $filter)

            @if($filter->project->name&&Session::get('lang')=='en')
                    <div class="post col-3">
                        <div class="post-img rsize-img">
                            @php
                                $m_img = substr(strstr($filter->project->main_img,'ds/'),3);
                            @endphp
                            <a href="/project/{{$filter->project->id}}"><img src="/uploads/crop_585x365_{{$m_img}}" alt="{{$filter->project->name}}"></a>
                        </div>
                        <div class="post-gallery rsize-img">
                            @foreach($filter->project->gallery as $g)
                                @php
                                    $g_img = substr(strstr($g->img_url,'ds/'),3);
                                @endphp
                                <div class="col-3"><a href="/project/{{$filter->project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$filter->project->name}}"></a></div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                        <div class="post-description">
                            <a href="/project/{{$filter->project->id}}"><h5>{{$filter->project->name}}</h5></a>
                            <a href="/architects/{{$filter->project->architect->id}}"><h6>{{$filter->project->architect->name}}</h6></a>
                            <p>
                                {{$filter->project->description}}
                            </p>
                        </div>
                    </div>
            @elseif($filter->project->name_ru&&Session::get('lang')=='ru')
                    <div class="post col-3">
                        <div class="post-img rsize-img">
                            @php
                                $m_img = substr(strstr($filter->project->main_img,'ds/'),3);
                            @endphp
                            <a href="/project/{{$filter->project->id}}"><img src="/uploads/crop_585x365_{{$m_img}}" alt="{{$filter->project->name_ru}}"></a>
                        </div>
                        <div class="post-gallery rsize-img">
                            @foreach($filter->project->gallery as $g)
                                @php
                                    $g_img = substr(strstr($g->img_url,'ds/'),3);
                                @endphp
                                <div class="col-3"><a href="/project/{{$filter->project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$filter->project->name}}"></a></div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                        <div class="post-description">
                            <a href="/project/{{$filter->project->id}}"><h5>{{$filter->project->name}}</h5></a>
                            <a href="/architects/{{$filter->project->architect->id}}"><h6>{{$filter->project->architect->name}}</h6></a>
                            <p>
                                {{$filter->project->description_ru}}
                            </p>
                        </div>
                    </div>
            @endif

        @endforeach

        <div class="clearfix"></div>
        @if($filters->count()>12)
                <div class="text-center">
                    {!! $filters->links() !!}
                </div>
        @else
            <div style="padding-bottom: 89px"></div>
        @endif
    </section>
@endsection
