@extends('layouts.main')

@section('content')
    <section class="main-wrapper">
        <?php $lang = Session::get('lang') ?>
        @foreach($projects as $project)
            @if($lang=='en')
                <div class="post col-3">
                    <div class="post-img rsize-img">
                        @php
                            $m_img = substr(strstr($project->main_img,'ds/'),3);
                        @endphp
                        <a href="/project/{{$project->id}}"><img src="/uploads/crop_585x365_{{$m_img}}" alt="{{$project->name}}"></a>
                    </div>
                    <div class="post-gallery rsize-img">
                        @foreach($project->gallery as $g)
                            @php
                                $g_img = substr(strstr($g->img_url,'ds/'),3);
                            @endphp
                            <div class="col-3"><a href="/project/{{$project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$project->name}}"></a></div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-description">
                        <a href="/project/{{$project->id}}"><h5>{{$project->name}}</h5></a>
                        <a href="/architects/{{$project->architect->id}}"><h6>{{$project->architect->name}}</h6></a>
                        <p>
                            {{$project->description}}
                        </p>
                    </div>
                </div>
            @else
                <div class="post col-3">
                    <div class="post-img rsize-img">
                        @php
                            $m_img = substr(strstr($project->main_img,'ds/'),3);
                        @endphp
                        <a href="/project/{{$project->id}}"><img src="/uploads/crop_585x365_{{$m_img}}" alt="{{$project->name_ru}}"></a>
                    </div>
                    <div class="post-gallery rsize-img">
                        @foreach($project->gallery as $g)
                            @php
                                $g_img = substr(strstr($g->img_url,'ds/'),3);
                            @endphp
                            <div class="col-3"><a href="/project/{{$project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$project->name_ru}}"></a></div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-description">
                        <a href="/project/{{$project->id}}"><h5>{{$project->name_ru}}</h5></a>
                        <a href="/architects/{{$project->architect->id}}">
                            <h6>
                                @if($project->architect->name_ru)
                                    {{$project->architect->name}}
                                @else
                                    {{$project->architect->name}}
                                @endif
                            </h6>
                        </a>
                        <p>
                            {{$project->description_ru}}
                        </p>
                    </div>
                </div>
            @endif
        @endforeach
        <div class="clearfix"></div>

        {{--{{$projects->nextPageUrl()}}--}}
            <div class="text-center">
                {!! $projects->links() !!}
            </div>
    </section>
@endsection
