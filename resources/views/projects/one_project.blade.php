<?php
        $ru = 'name_ru';
        $en = 'name';
        $lang = Session::get('lang');
        $site_url = $_SERVER['SERVER_NAME'];
    if ($projects->$$lang==''){
        header("Location: http://$site_url");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Architecture Project</title>
    <link rel="stylesheet" type="text/css" href="{{url('/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/css/style.css')}}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css" rev="stylesheet">
        /*Preloader*/
        .preloader{
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: white;
            z-index: 9000;
            opacity: 1;
            width: 100%;
        }
    </style>
    {{--GoogleAnalytics--}}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-91158171-1', 'auto');
        ga('send', 'pageview');

    </script>
    {{-- End GoogleAnalytics--}}
</head>
<body>
<div class="preloader">
    <div class="line"></div>
</div>
{{app()->setLocale(Session::get('lang'))}}

@if(Auth::check())
    <a href="{{url('admin/project/'.$projects->id)}}" class="change_content_button">
        <div>
            <img src="/svg/vector.svg">
        </div>
    </a>
@endif

<div class="slide-burger-menu">


    {{--SOCIAL BUTTONS--}}

    {{--<div class="social-menu">--}}
    {{--<a href="#"><div></div><img src="/svg/facebook-logo.svg"></a>--}}
    {{--<a href="#"><img src="/svg/google-plus.svg"></a>--}}
    {{--<a href="#"><div></div><img src="/svg/instagram-logo.svg"></a>--}}
    {{--<a href="#"><div></div><img src="/svg/vk-logo.svg"></a>--}}
    {{--</div>--}}


    <div class="menu">
        <ul>
            {{app()->setLocale(Session::get('lang'))}}
            <li><a href="{{url('/filter/house')}}">{{trans('language.House')}}</a></li>
            <li><a href="{{url('/filter/apartments')}}">{{trans('language.Apartments')}}</a></li>
            <li><a href="{{url('/filter/restaurants')}}">{{trans('language.Restaurants')}}</a></li>
            <li><a href="{{url('/filter/hotels')}}">{{trans('language.Hotels')}}</a></li>
            <li><a href="{{url('/filter/offices')}}">{{trans('language.Offices')}}</a></li>
            {{--<li><a href="{{url('/filter/studio')}}">{{trans('language.Studio')}}</a></li>--}}
            {{--<li><a href="#">{{trans('language.Contacts')}}</a></li>--}}
        </ul>
        {{--<div class="button-follow add-project">--}}
        {{--<a href="#">{{trans('language.Add Project')}}</a>--}}
        {{--</div>--}}
    </div>
</div>
<header>
    <div class="project-carusel">
        <div id="owl-example" class="loop owl-carousel">
                <div class="item"><img src="{{$projects->main_img}}" alt="{{$projects->name}}"></div>
            @foreach($imgResize as $img)
                @php

                    /*$c_img=strstr($img->img_url,'.jpg',true);
                    if(file_exists($_SERVER['DOCUMENT_ROOT'].$img->img_url)){
                        $c_img= $img->img_url;
                    }*/
                @endphp
                <div class="item"><img src="/uploads/crop_h800_{{$img->basename}}" alt="{{$projects->name}}"></div>
            @endforeach
        </div>
    </div>
    <section class="padding-0-55" style="position: absolute;top: 0;right: 0;left: 0">
        <div class="header-logo">
            <a href="/">
                <img src="{{url('/svg/logo-A.svg')}}" alt="architecture">
            </a>
        </div>

        {{--SEARCH--}}

        {{--<div class="header-search">--}}
        {{--<form>--}}
        {{--<input type="text">--}}
        {{--</form>--}}
        {{--<img src="/svg/search-A.svg" alt="serach">--}}
        {{--</div>--}}


        <div class="burger-menu">
            <div class="menu-trigger second">
                <span class="line line-1"></span>
                <span class="line line-2"></span>
                <span class="line line-3"></span>
            </div>
        </div>
        <div class="language">
            <div class="lang">
                @if (Session::has('lang'))
                    {{Session::get('lang')}}
                @else
                    EN
                @endif
            </div>
            <div class="chose-lang">
                <a href="{{url('/lang/en')}}">EN</a>
                <a href="{{url('/lang/ru')}}">RU</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
</header>
@if(Session::get('lang')=='en')
    <main>
        <section class="about-project">
            <div class="project-company resize-text">
                <img src="{{$projects->architect->logo_url}}" alt="archi">
                <div>
                    <h4><a href="/architects/{{$projects->architect->id}}">{{$projects->architect->name}}</a></h4>
                    <p class="pb-68">{!! nl2br($projects->architect->description) !!}</p>

                    {{--<div class="button-follow follow">--}}
                        {{--<a href="#">{{trans('language.Follow')}}</a>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="project-short-decription">
                @if($projects->architects)
                    <ul>
                        <li>{{trans('language.Architects')}}</li>
                        <li>{{$projects->architects}}</li>
                    </ul>
                @endif
                @if($projects->project_team)
                    <ul>
                        <li>{{trans('language.Project Team')}}</li>
                        <li>{{$projects->project_team}}</li>
                    </ul>
                @endif
                @if($projects->area)
                    <ul>
                        <li>{{trans('language.Area')}}</li>
                        <li>{{$projects->area}} {{trans('language.sqm')}}</li>
                    </ul>
                @endif
                @if($projects->year)
                    <ul>
                        <li>{{trans('language.Project Year')}}</li>
                        <li>{{$projects->year}}</li>
                    </ul>
                @endif
                @if($projects->photographer)
                    <ul>
                        <li>{{trans('language.Photographs')}}</li>
                        <li>{{$projects->photographer}}</li>
                    </ul>
                @endif
            </div>
            <div class="project-decription">
                <h2>{{$projects->name}}</h2>
                <h3>{{$projects->country}}</h3>
                <p class="pb-68">
                    {!! nl2br($projects->description) !!}
                </p>
                {{--<div class="button-follow save-project">--}}
                    {{--<a href="#">{{trans('language.Save project')}}</a>--}}
                {{--</div>--}}
            </div>
            <div class="clearfix"></div>
        </section>
        <section class="more-header padding-0-55">
            <h3>{{trans('language.Continue viewing')}}</h3>
        </section>
        <section class="more-projects padding-0-55">
            @foreach($projects_one_page as $key=>$project)
                @if($key != 6)
                <div class="post col-3">
                    <div class="post-img rsize-img">
                        <a href="/project/{{$project->id}}"><img src="{{$project->main_img}}" alt="{{$project->name}}"></a>
                    </div>
                    <div class="post-gallery rsize-img">
                        @foreach($project->gallery as $g)
                            @php
                                $g_img = substr(strstr($g->img_url,'ds/'),3);
                            @endphp
                            <div class="col-3"><a href="/project/{{$project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$project->name_ru}}"></a></div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-description">
                        <a href="/project/{{$project->id}}"><h5>{{$project->name}}</h5></a>
                        <a href="/architects/{{$project->architect->id}}"><h6>{{$project->architect->name}}</h6></a>
                        <p>
                            {{$project->description}}
                        </p>
                    </div>
                </div>
                @endif
            @endforeach
            <div class="clearfix"></div>
        </section>
        @if(count($projects_one_page)>6)
            <section class="add-more-projects pb-68">
                <div class="button-follow follow">
                    <a>{{trans('language.More projects')}}</a>
                </div>
            </section>
        @endif
    </main>
@else
    <main>
        <section class="about-project">
            <div class="project-company resize-text">
                <img src="{{$projects->architect->logo_url}}" alt="archi">
                <div>
                    <h4><a href="/architects/{{$projects->architect->id}}">{{$projects->architect->name}}</a></h4>
                    <p class="pb-68">{!! nl2br($projects->architect->description_ru) !!}</p>

                    {{--<div class="button-follow">--}}
                        {{--<a href="#">{{trans('language.Follow')}}</a>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="project-short-decription">
                @if($projects->architects_ru)
                    <ul>
                        <li>{{trans('language.Architects')}}</li>
                        <li>{{$projects->architects_ru}}</li>
                    </ul>
                @endif
                @if($projects->project_team_ru)
                    <ul>
                        <li>{{trans('language.Project Team')}}</li>
                        <li>{{$projects->project_team_ru}}</li>
                    </ul>
                @endif
                @if($projects->area)
                    <ul>
                        <li>{{trans('language.Area')}}</li>
                        <li>{{$projects->area}} {{trans('language.sqm')}}</li>
                    </ul>
                @endif
                @if($projects->year)
                    <ul>
                        <li>{{trans('language.Project Year')}}</li>
                        <li>{{$projects->year}}</li>
                    </ul>
                @endif
                @if($projects->photographer_ru)
                    <ul>
                        <li>{{trans('language.Photographs')}}</li>
                        <li>{{$projects->photographer_ru}}</li>
                    </ul>
                @endif
            </div>
            <div class="project-decription">
                <h2>{{$projects->name_ru}}</h2>
                <h3>{{$projects->country}}</h3>
                <p class="pb-68">
                    {!! nl2br($projects->description_ru) !!}
                </p>
                {{--<div class="button-follow">--}}
                    {{--<a href="#">{{trans('language.Save project')}}</a>--}}
                {{--</div>--}}
            </div>
            <div class="clearfix"></div>
        </section>
        <section class="more-header padding-0-55">
            <h3>{{trans('language.Continue viewing')}}</h3>
        </section>
        <section class="more-projects padding-0-55">

            @foreach($projects_one_page as $key=> $project)
                @if($key != 6)
                <div class="post col-3" data-id="{{$project->id}}">
                    <div class="post-img rsize-img">
                        <a href="/project/{{$project->id}}"><img src="{{$project->main_img}}" alt="{{$project->name_ru}}"></a>
                    </div>
                    <div class="post-gallery rsize-img">
                        @foreach($project->gallery as $g)
                            @php
                                $g_img = substr(strstr($g->img_url,'ds/'),3);
                            @endphp
                            <div class="col-3"><a href="/project/{{$project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$project->name_ru}}"></a></div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-description">
                        <a href="/project/{{$project->id}}"><h5>{{$project->name_ru}}</h5></a>
                        <a href="/architects/{{$project->architect->id}}"><h6>{{$project->architect->name}}</h6></a>
                        <p>
                            {{$project->description_ru}}
                        </p>
                    </div>
                </div>
                @endif
            @endforeach
            <div class="clearfix"></div>
        </section>
        @if(count($projects_one_page)>6)
            <section class="add-more-projects pb-68">
                <div class="button-follow follow">
                    <a>{{trans('language.More projects')}}</a>
                </div>
            </section>
        @endif
    </main>
@endif
<div class="gallery">
    <div class="gallery-bg"></div>
    <div class="gallery-slide">
        <div class="gallery-img"></div>
        <div class="gallery-prev gal-ar" data-arrow="prev">
            <img src="/svg/nav-next.svg">
        </div>
        <div class="gallery-next gal-ar" data-arrow="next">
            <img src="/svg/nav-next.svg">
        </div>
    </div>
    {{--<div class="gallery-nav">--}}
       {{----}}
    {{--</div>--}}
    {{--<div class="gallery-close">--}}
        {{--<div class="close-icon">--}}
            {{--<img src="/svg/cross-out_white.svg">--}}
        {{--</div>--}}
    {{--</div>--}}
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{url('/js/owl.carousel.js')}}"></script>
<script src="{{url('/js/script.js')}}"></script>
</body>
</html>