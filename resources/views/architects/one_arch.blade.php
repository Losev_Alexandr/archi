<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Architecture Company</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--GoogleAnalytics--}}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-91158171-1', 'auto');
        ga('send', 'pageview');

    </script>
    {{-- End GoogleAnalytics--}}
</head>
<body>
{{app()->setLocale(Session::get('lang'))}}



@if(Auth::check())
    <a href="{{url('admin/company/'.$arch->id)}}" class="change_content_button">
        <div>
            <img src="/svg/vector.svg">
        </div>
    </a>
@endif
<div class="slide-burger-menu">


    {{--SOCIAL BUTTONS--}}

    {{--<div class="social-menu">--}}
    {{--<a href="#"><div></div><img src="/svg/facebook-logo.svg"></a>--}}
    {{--<a href="#"><img src="/svg/google-plus.svg"></a>--}}
    {{--<a href="#"><div></div><img src="/svg/instagram-logo.svg"></a>--}}
    {{--<a href="#"><div></div><img src="/svg/vk-logo.svg"></a>--}}
    {{--</div>--}}


    <div class="menu">
        <ul>
            {{app()->setLocale(Session::get('lang'))}}
            <li><a href="{{url('/filter/house')}}">{{trans('language.House')}}</a></li>
            <li><a href="{{url('/filter/apartments')}}">{{trans('language.Apartments')}}</a></li>
            <li><a href="{{url('/filter/restaurants')}}">{{trans('language.Restaurants')}}</a></li>
            <li><a href="{{url('/filter/hotels')}}">{{trans('language.Hotels')}}</a></li>
            <li><a href="{{url('/filter/offices')}}">{{trans('language.Offices')}}</a></li>
            {{--<li><a href="{{url('/filter/studio')}}">{{trans('language.Studio')}}</a></li>--}}
            {{--<li><a href="#">{{trans('language.Contacts')}}</a></li>--}}
        </ul>
        {{--<div class="button-follow add-project">--}}
        {{--<a href="#">{{trans('language.Add Project')}}</a>--}}
        {{--</div>--}}
    </div>
</div>
<header class="company" style="background: url('{{$arch->main_img_url}}')">
    <section class="padding-0-55">
        <div class="header-logo">
            <a href="/">
                <img src="{{url('/svg/logo-A.svg')}}" alt="architecture">
            </a>
        </div>


        {{--SEARCH--}}

        {{--<div class="header-search">--}}
        {{--<form>--}}
        {{--<input type="text">--}}
        {{--</form>--}}
        {{--<img src="/svg/search-A.svg" alt="serach">--}}
        {{--</div>--}}


        <div class="burger-menu">
            <div class="menu-trigger second">
                <span class="line line-1"></span>
                <span class="line line-2"></span>
                <span class="line line-3"></span>
            </div>
        </div>
        <div class="language">
            <div class="lang">
                @if (Session::has('lang'))
                    {{Session::get('lang')}}
                @else
                    EN
                @endif
            </div>
            <div class="chose-lang">
                <a href="{{url('/lang/en')}}">EN</a>
                <a href="{{url('/lang/ru')}}">RU</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
</header>
@if(Session::get('lang')=='en')
    <main>
        <section class="architecture-company">
            <div class="company-header">
                <h3>{{$arch->name}}</h3>
                <h4><a>{{$arch->country}}</a></h4>
            </div>
            {{--<div class="button-follow follow">--}}
                {{--<a href="#">Follow</a>--}}
            {{--</div>--}}
            <div class="clearfix"></div>
        </section>
        <section class="about-company">
            <div class="architects-img">
                <img src="{{$arch->logo_url}}" alt="{{$arch->name}}">
            </div>
            <div class="architects-contact">
                {{$arch->country}}
                <br/>
                <br/>
                <span>{!! nl2br($arch->phone_number) !!}</span><br/>
                <a href="{{$arch->site_url}}" target="_blank">{{$arch->site_url}}</a>
            </div>
            <div class="architects-decription resize-text">
                <p>
                    {!! nl2br($arch->description) !!}
                    {{--<span class="more">... more</span>--}}
                </p>
            </div>
            <div class="clearfix"></div>
        </section>
        <section class="more-header padding-0-55">
            <h3>{{trans('language.Projects')}} {{$arch->name}}</h3>
        </section>
        <section class="more-projects padding-0-55">
            @foreach($projects as $project)
                <div class="post col-3" data-id="{{$project->id}}">
                    <div class="post-img rsize-img">
                        <a href="/project/{{$project->id}}"><img src="{{$project->main_img}}" alt="{{$project->name}}"></a>
                    </div>
                    <div class="post-gallery rsize-img">
                        @foreach($project->gallery as $g)
                            @php
                                $g_img = substr(strstr($g->img_url,'ds/'),3);
                            @endphp
                            <div class="col-3"><a href="/project/{{$project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$project->name_ru}}"></a></div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-description">
                        <a href="/project/{{$project->id}}"><h5>{{$project->name}}</h5></a>
                        <a href="/architects/{{$project->architect->id}}"><h6>{{$project->architect->name}}</h6></a>
                        <p>
                            {{$project->description}}
                        </p>
                    </div>
                </div>
            @endforeach
            <div class="clearfix"></div>
        </section>
        <section class="add-more-projects-studio">
            <div class="button-follow follow">
                <a data-studio-id="{{$arch->id}}">{{trans('language.More projects')}}</a>
            </div>
        </section>
    </main>
@else
    <main>
        <section class="architecture-company">
            <div class="company-header">
                <h3>{{$arch->name}}</h3>
                <h4><a>{{$arch->country}}</a></h4>
            </div>
            {{--<div class="button-follow">--}}
                {{--<a href="#">{{trans('language.Follow')}}</a>--}}
            {{--</div>--}}
            <div class="clearfix"></div>
        </section>
        <section class="about-company">
            <div class="architects-img">
                <img src="{{$arch->logo_url}}" alt="{{$arch->name}}">
            </div>
            <div class="architects-contact">
                {{$arch->country}}
                <br/>
                <br/>
                <span>{!! nl2br($arch->phone_number) !!}</span><br/>
                <a href="{{$arch->site_url}}" target="_blank">{{$arch->site_url}}</a>
            </div>
            <div class="architects-decription resize-text">
                <p>
                    {!! nl2br($arch->description_ru) !!}
                </p>
            </div>
            <div class="clearfix"></div>
        </section>
        <section class="more-header padding-0-55">
            <h3>{{trans('language.Projects')}} {{$arch->name}}</h3>
        </section>
        <section class="more-projects padding-0-55">
            @foreach($projects as $project)
                <div class="post col-3" data-id="{{$project->id}}">
                    <div class="post-img rsize-img">
                        <a href="/project/{{$project->id}}"><img src="{{$project->main_img}}" alt="{{$project->name}}"></a>
                    </div>
                    <div class="post-gallery rsize-img">
                        @foreach($project->gallery as $g)
                            @php
                                $g_img = substr(strstr($g->img_url,'ds/'),3);
                            @endphp
                            <div class="col-3"><a href="/project/{{$project->id}}"><img src="/uploads/crop_190x120_{{$g_img}}" alt="{{$project->name_ru}}"></a></div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-description">
                        <a href="/project/{{$project->id}}"><h5>{{$project->name}}</h5></a>
                        <a href="/architects/{{$project->architect->id}}"><h6>{{$project->architect->name}}</h6></a>
                        <p>
                            {{$project->description_ru}}
                        </p>
                    </div>
                </div>
            @endforeach
            <div class="clearfix"></div>
        </section>
        <section class="add-more-projects-studio">
            <div class="button-follow follow">
                <a data-studio-id="{{$arch->id}}">{{trans('language.More projects')}}</a>
            </div>
        </section>
    </main>
@endif
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/js/script.js"></script>
</body>
</html>